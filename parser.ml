type sudoku = int list list


(*Read a .sdk files and return a list of matrix.*)
let file_to_mat : string -> sudoku list = fun file ->
    let rec ltm mat line = function (*Read a ligne an return a matrix.*)
        |n when n = String.length(line) -> List.rev mat
        |n -> ltm ((ltl (String.sub line n 9) 0)::mat) line (n+9)
    and ltl line = function (*Read 9 digits on a row to return a vector.*)
        |9 -> []
        |n -> (int_of_char line.[n] - 48)::(ltl line (n+1)) in

    let rec ftm = function (*Returns the list of matrix.*)
        |[]->[]
        |t::q -> (ltm [] t 0)::ftm q in

    let ic = open_in file in
    let lines = ref [] in
    try
         while true; do
            lines := input_line ic::!lines
         done;
         []
    with 
        |End_of_file -> close_in ic; (*At the end of the file, closed it and return the list sorted.*)
                        ftm (List.rev !lines)
        |e -> close_in ic;
              []


