# SAT_Solver

There are severals possibilities to compile:

 - 'make solver' compile the solver which take a .dimacs file and write the solution on a file called solution.
 - 'make sudoku' compile the sudoku solver wich take either a number and solve one sudoku or all and solve all sudoku. 
   Solutions are writen in solutions.sdk. It call the solver to solve sudokus.
 - 'make printer' compile the printer which take a number and print the corresponding sudoku and solution. 
   (if all sudokus are not solve it can print the wrong solution.)
 - 'make all' (or juste make) compile the three programs.
 - 'make test' call make all an then solve all sudokus.
 - 'make clean' delete all compile files.


To run a compiled program 'prog', just write './prog arg'.



