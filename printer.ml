type sudoku = int list list

(*Print a ligne of a sudoku.*)
let rec print_line : int list -> unit = function
    |[] -> Printf.printf("|\n")
    |t::q -> if (List.length q + 1) mod 3 = 0
             then Printf.printf("|");
             Printf.printf "%d" t;
             print_line q

(*Print a sudoku.*)
let rec print_sudoku : sudoku -> unit  = function 
    |[] -> Printf.printf("-------------\n")
    |t::q -> if (List.length q + 1) mod 3 = 0
             then Printf.printf("-------------\n");
             print_line t;
             print_sudoku q

(*Read a .sdk files and return a list of matrix.*)
let file_to_mat : string -> sudoku list = fun file ->
    let rec ltm mat line = function (*Read a ligne an return a matrix.*)
        |n when n = String.length(line) -> List.rev mat
        |n -> ltm ((ltl (String.sub line n 9) 0)::mat) line (n+9)
    and ltl line = function (*Read 9 digits on a row to return a vector.*)
        |9 -> []
        |n -> (int_of_char line.[n] - 48)::(ltl line (n+1)) in

    let rec ftm = function (*Returns the list of matrix.*)
        |[]->[]
        |t::q -> (ltm [] t 0)::ftm q in

    let ic = open_in file in (*At the end of the file, closed it and return the list sorted.*)
    let lines = ref [] in
    try
         while true; do
            lines := input_line ic::!lines
         done;
         []
    with 
        |End_of_file -> close_in ic;
                        ftm (List.rev !lines)
        |e -> close_in ic;
              []

(*Return the nth sudokus from a list of sudokus.*)
let rec get_sudoku n sudokus = match n,sudokus with
    | n,[] -> []
    | 1, t::q -> t
    | n, t::q -> get_sudoku (n-1) q

(*Main function wich take a number and print the nth sudoku and the nth solution.*)
let main : unit -> unit = fun () ->
    let n = int_of_string Sys.argv.(1) in
    let empty = file_to_mat "puzzles.sdk" in
    let sol = file_to_mat "solutions.sdk" in
    let empty_sudo = get_sudoku n empty in
    let sol_sudo = get_sudoku n sol in
    Printf.printf "Grille:\n";
    print_sudoku empty_sudo;
    Printf.printf "\nSolution:\n";
    print_sudoku sol_sudo

let _ = main()
    
