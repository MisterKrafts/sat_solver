type var = int

type lit = int

type model = int list

module Clause = Set.Make(struct type t = var let compare = compare end)

module Cnf = Set.Make(struct type t = Clause.t let compare = Clause.compare end)

type t =
  {
    nb_var : int;
    nb_clause : int;
    cnf : Cnf.t
  }

let neg var = -var
