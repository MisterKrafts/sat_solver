(*Remove list is use to remove a list of literal with a fixed value from a cnf. *)
let rec remove_list (p : Ast.Cnf.t) (li : Ast.lit list) : Ast.Cnf.t =
    let units = Ast.Clause.of_list li in
    Ast.Cnf.fold (fun c cnf ->
        if not (Ast.Clause.is_empty (Ast.Clause.inter c units)) then cnf
        else Ast.Cnf.add (Ast.Clause.diff c (Ast.Clause.map ((-) 0) units)) cnf) p
            Ast.Cnf.empty

(*Test if a cnf contains an empty clause.*)
let empty_clause : Ast.Cnf.t -> bool = fun p ->
    Ast.Cnf.mem (Ast.Clause.empty) p

(*Returns a list of literals alone in a clause.*)
let singleton : Ast.Cnf.t -> int list  = fun p ->
    Ast.Cnf.fold (fun c li -> if (Ast.Clause.cardinal c = 1) then (Ast.Clause.choose c)::li else li) p []

(*Return the maximal element of a list.*)
let list_max li = List.fold_left (fun a b -> max (max a (-a)) (max b (-b))) 0 li

(*Return true if the list of literals contains x and -x*)
let abort : Ast.lit list -> bool = fun lit_list ->
    let n = list_max lit_list in let mem = Array.make (n+1) 0 in
    let rec abort_r = function
        | [] -> false
        | t::q when t > 0 -> if (mem.(t) = -1) then true else begin mem.(t) <- 1; abort_r q end
        | t::q -> if (mem.(-t) = 1) then true else begin mem.(-t) <- -1; abort_r  q end
    in abort_r lit_list

(*From a cnf return None if it is not satisfiable and Some of a model if it is satisfiable.*)
let rec solve_r : Ast.Cnf.t ->  Ast.model option  = fun p ->
    if (empty_clause p) then None else begin (*If there is an empty clause it is not satisfiable.*)
        if (Ast.Cnf.is_empty p) then Some([]) (*If there is no clause, the empty model is return.*)
        else begin
            let lit_list = singleton p in (*Take all literals which can be assigned.*)
            let abort_b = abort lit_list in 
            if abort_b then None (*If x and -x are forced then it is not satisfiable.*)
            else begin match lit_list with
                | [] -> begin let x = abs (Ast.Clause.choose (Ast.Cnf.choose p)) in (*If there is no literals alone, we choose one to assigned it.*)
                        match solve_r (remove_list p [x]) with
                            | None -> solve_r (remove_list p [-x]) (*if not satisfiable, then we change the value.*)
                            | Some(l) -> Some(x::l)
                        end
                | li -> begin match solve_r (remove_list p li) with (*We remove the literals with fixed value from the cnf.*)
                            | None -> None
                            | Some(l) -> Some((List.filter ((<) 0) li)@l) (*We merge the models and keep only positive literals.*)
                        end
            end
        end
    end

(*Solve a Sat problem.*)
let solve : Ast.t -> Ast.model option = fun p ->
    solve_r p.cnf
