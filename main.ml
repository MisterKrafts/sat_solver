(*Return the nth sudokus from a list of sudokus.*)
let rec get_sudoku n sudokus = match n,sudokus with
    | n,[] -> []
    | 1, t::q -> t
    | n, t::q -> get_sudoku (n-1) q

(*Call the solver on a list of sudokus.*)
let rec call : Parser.sudoku list -> unit = function
    |[] -> ()
    |t::q -> Sudoku.to_dimacs t;
             let n = Sys.command "./solver sudoku.dimacs" in
             if n = 0 then 
             Sudoku.sol_to_sudoku "solution";
             call q

(*Main function which solve a unique sudoku or all sudokus.*)
let main : unit -> unit = fun () ->
    let sudokus = Parser.file_to_mat "puzzles.sdk" in
    if Sys.argv.(1) = "all" then call sudokus else begin
    let sudoku = get_sudoku (int_of_string Sys.argv.(1)) sudokus in Sudoku.to_dimacs sudoku;
    let n = Sys.command "./solver sudoku.dimacs" in
    if n = 0 then 
    Sudoku.sol_to_sudoku "solution" end


let _ = main()
