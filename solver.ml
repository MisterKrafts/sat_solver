let sl_t_il : string list -> int list = fun sli -> List.fold_right (fun s li -> if s = "0" then li else (int_of_string s)::li) sli []

(*Read a .dimacs file a return a cnf.*)
let dimacs_to_cnf : string -> Ast.t = fun file ->
    let ic = open_in file in
    let line = ref "" in
    let b = ref false in
    let clauses = ref [] in
    let nb_var = ref 0 in
    let nb_cl = ref 0 in
    try
        line := input_line ic;
        while !line.[0] = 'c'; do
            line := input_line ic
        done;
        let lits = ref (String.split_on_char ' ' !line) in
        nb_var := int_of_string (List.hd (List.tl (List.tl !lits)));
        nb_cl := int_of_string (List.hd (List.tl (List.tl (List.tl !lits))));
        b := true;
        while true; do
                line := input_line ic;
                lits := String.split_on_char ' ' !line;
                clauses := (Ast.Clause.of_list (sl_t_il !lits))::!clauses
        done;
        {nb_var= !nb_var; nb_clause= !nb_cl; cnf=Ast.Cnf.of_list !clauses}
    with
         |End_of_file -> close_in ic;
                         if not !b then begin (*If the dimacs is not complete.*)
                             Printf.printf "%s\n" "Dimacs file too short";
                             {nb_var=0; nb_clause=0; cnf=Ast.Cnf.empty} end
                         else
                             {nb_var= !nb_var; nb_clause= !nb_cl; cnf=Ast.Cnf.of_list !clauses}
         |e -> close_in ic;
               {nb_var=0; nb_clause=0; cnf=Ast.Cnf.empty}

(*Call the dpll and print the solution on a file.*)
let solve : string -> unit = fun file ->
    let cnf = dimacs_to_cnf file in
    let sat = Dpll.solve cnf in
    let oc = open_out "solution" in
    match sat with 
        | None -> Printf.fprintf oc "UNSAT\n";
        | Some(m) -> begin Printf.fprintf oc "SAT\n";
                     let print_lit lit = Printf.fprintf oc "%d " lit in
                     List.iter print_lit (List.sort compare m); 
                     Printf.fprintf oc "0\n" end;
    close_out oc

(*Main function which take a name of file.*)
let main : unit -> unit = fun () ->
        let file = Sys.argv.(1) in solve file


let _ = main()
