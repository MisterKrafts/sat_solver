OBJS := ast.ml dpll.ml solver.ml
OBJ := ast.ml parser.ml sudoku.ml main.ml

all: solver sudoku print

solver: $(OBJS)
	ocamlopt -o solver $(OBJS)

sudoku: $(OBJ)
	ocamlopt -o sudoku_solver unix.cmxa $(OBJ)

print: printer.ml
	ocamlopt -o printer printer.ml

test: all
	./sudoku_solver all

clean:
	rm -f *.cmo *.cmi *.o *.cmx solver *.dimacs solution solutions.sdk sudoku_solver printer
