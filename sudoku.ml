type t = int list

let size = 9

(*Transorm a string list to an int list.*)
let sl_t_il : string list -> int list = fun sli -> List.fold_right (fun s li -> if s = "0" then li else (int_of_string s)::li) sli []

(*Transform a sudoku in cnf. The sudoku is given in one line by an int list.*)
let to_cnf : t -> Ast.t = fun sudoku ->

  let bij i j k = size*size*i + size * j + k in (*Bijection from position and value to number of the variable*)

  let bij_s p q = p / 3 * 3 +q /3, p mod 3 * 3 + q mod 3 in 

  let rec line i j1 j2 k = match i,j1,j2,k with  (*A line is valid.*)
    | 0, 1, 0, 1 -> [[-(bij 0 1 1); -(bij 0 0 1)]]
    | 0, 1, 0, k -> [-(bij 0 1 k); -(bij 0 0 k)]::(line (size-1) (size-1) (size -2) (k-1))
    | i, 1, 0, k -> [-(bij i 1 k); -(bij i 0 k)]::(line (i-1) (size-1) (size-2) k)
    | i, j1, 0, k -> [-(bij i j1 k); -(bij i 0 k)]::(line i (j1-1) (j1-2) k)
    | i, j1, j2, k-> [-(bij i j1 k); -(bij i j2 k)]::(line i j1 (j2-1) k) in

  let rec col i1 i2 j k = match i1,i2,j,k with (*A colum is valid.*)
    | 1, 0, 0, 1 -> [[-(bij 1 0 1); -(bij 0 0 1)]]
    | 1, 0, 0, k -> [-(bij 1 0 k); -(bij 0 0 k)]::(col (size-1) (size-2) (size-1) (k-1))
    | 1, 0, j, k -> [-(bij 1 j k); -(bij 0 j k)]::(col (size-1) (size-2) (j-1) k)
    | i1, 0, j, k -> [-(bij i1 j k); -(bij 0 j k)]::(col (i1-1) (i1-2) j k)
    | i1, i2, j, k -> [-(bij i1 j k); -(bij i2 j k)]::(col i1 (i2-1) j k) in

  let rec square i j k1 k2 = match i,j,k1,k2 with (*A cell contains only one value.*)
    | 0, 0, 2, 1 -> [[-(bij 0 0 2); -(bij 0 0 1)]]
    | i, 0, 2, 1 -> [-(bij i 0 2); -(bij i 0 1)]::(square (i-1) (size-1) size (size-1))
    | i, j, 2, 1 -> [-(bij i j 2); -(bij i j 1)]::(square i (j-1) size (size-1))
    | i, j, k1, 1 -> [-(bij i j k1); -(bij i j 1)]::(square i j (k1-1) (k1-2))
    | i, j, k1, k2 -> [-(bij i j k1); -(bij i j k2)]::(square i j k1 (k2-1)) in

  let rec big_square p q1 q2 k = match p,q1,q2,k with (*A square is valid*)
    | 0, 1, 0, 1 -> let (i1,j1),(i2,j2) = (bij_s 0 1),(bij_s 0 0) in
                    [[-(bij i1 j1 1); -(bij i2 j2 1)]]
    | 0, 1, 0, k -> let (i1,j1),(i2,j2) = (bij_s 0 1),(bij_s 0 0) in 
                    [-(bij i1 j1 k); -(bij i2 j2 k)]::(big_square (size-1) (size-1) (size-2) (k-1))
    | p, 1, 0, k -> let (i1,j1),(i2,j2) = (bij_s p 1),(bij_s p 0) in
                    [-(bij i1 j1 k); -(bij i2 j2 k)]::(big_square (p-1) (size-1) (size-2) k)
    | p, q1, 0, k -> let (i1,j1),(i2,j2) = (bij_s p q1),(bij_s p 0) in
                     [-(bij i1 j1 k); -(bij i2 j2 k)]::(big_square p (q1-1) (q1-2) k)
    | p, q1, q2, k-> let (i1,j1),(i2,j2) = (bij_s p q1),(bij_s p q2) in
                     [-(bij i1 j1 k); -(bij i2 j2 k)]::(big_square p q1 (q2-1) k) in
  
  (*A cell contains a value.*)
  let rec exist i j = match i,j with
    | -1,_ -> []
    | i, 0 -> [bij i 0 1; bij i 0 2; bij i 0 3; bij i 0 4; bij i 0 5; bij i 0 6; bij i 0 7; bij i 0 8; bij i 0 9]::(exist (i-1) (size-1))
    | i, j -> [bij i j 1; bij i j 2; bij i j 3; bij i j 4; bij i j 5; bij i j 6; bij i j 7; bij i j 8; bij i j 9]::(exist i (j-1)) in
  
  (*Read given numbers.*)
  let rec init n = function
    | [] -> []
    | t::q when t>0 -> [bij (n/9) (n mod 9) t]::(init (n+1) q)
    | t::q -> init (n+1) q in
  
  let l = Ast.Cnf.of_list (List.map (fun p -> Ast.Clause.of_list p) (line (size-1) (size-1) (size-2) size)) in
  let c = Ast.Cnf.of_list (List.map (fun p -> Ast.Clause.of_list p) (col (size-1) (size-2) (size-1) size)) in
  let s = Ast.Cnf.of_list (List.map (fun p -> Ast.Clause.of_list p) (square (size-1) (size-1) size (size-1))) in
  let bs = Ast.Cnf.of_list (List.map (fun p -> Ast.Clause.of_list p) (big_square (size-1) (size-1) (size-2) size)) in
  let e = Ast.Cnf.of_list (List.map (fun p -> Ast.Clause.of_list p) (exist (size-1) (size-1))) in
  let i = Ast.Cnf.of_list (List.map (fun p -> Ast.Clause.of_list p) (init 0 sudoku)) in
  let cnf_f = Ast.Cnf.union(Ast.Cnf.union (Ast.Cnf.union (Ast.Cnf.union (Ast.Cnf.union l c) s) bs) e) i in
  {nb_var = size*size*size; nb_clause = (Ast.Cnf.cardinal cnf_f); cnf= cnf_f};;


(*Write the cnf on a .dimacs file.*)
let cnf_to_dimacs : Ast.t -> unit = fun cnf -> 
  let oc = open_out "sudoku.dimacs" in
  Printf.fprintf oc "p cnf %d %d\n" cnf.nb_var cnf.nb_clause;
  let print_var var = Printf.fprintf oc "%d " var in
  let print_clause clause = Ast.Clause.iter print_var clause;
                            Printf.fprintf oc "%d\n" 0 in
  let print_cnf cnf = Ast.Cnf.iter print_clause cnf in
  print_cnf cnf.cnf;
  close_out oc;;

(*Transform a matrix in cnf and then write it on a .dimacs file.*)
let to_dimacs : Parser.sudoku -> unit = fun sudoku ->
      let cnf = to_cnf (List.flatten sudoku) in
      cnf_to_dimacs cnf;;

(*Write the solution written on a given file in solutions.sdk.*)
let sol_to_sudoku : string -> unit = fun file ->
    let bij_1 n = let k = (n - 1) mod size + 1 in
                 let j = (n mod size*size - k)/size in
                 let i = (n - k - size *j)/size/size in
                 i,j,k in
    
    let get_val vars = List.fold_right (fun n li -> let i,j,k = bij_1 n in k::li) vars [] in

    let oc = open_out_gen [Open_append; Open_creat] 0o666 "solutions.sdk" in
    let ic = open_in file in
    let line = input_line ic in
    if line = "UNSAT" then begin 
        Printf.fprintf oc "0\n";
        close_out oc;
        close_in ic end
    else begin 
        let line2 = input_line ic in
        let vars = String.split_on_char ' ' line2 in
        let vals = get_val (sl_t_il vars) in
        let print_val valu = Printf.fprintf oc "%d" valu in
        List.iter print_val vals;
        Printf.fprintf oc "\n";
        close_out oc;
        close_in ic end
